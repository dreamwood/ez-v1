package gen

import "gitee.com/dreamwood/ez-go/maker"

func Ucache() {
	doc := maker.CreateDoc("Ucache", "用户缓存", "uc")
	doc.Add("uid", "UID").IsInt()
	doc.Add("key", "KEY").IsString()
	doc.Add("data", "数据").IsAny("ss.M")
	doc.Generate()
}
