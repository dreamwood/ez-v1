package uc

import (
	"context"
	"encoding/json"
	"ez/apps/uc/auto/mc"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"time"
)

type Ucache struct {
	mgo.BaseDoc `bson:"-" json:"-"`
	Id          int64      `json:"id" bson:"id,omitempty"`
	Uid         int64      `json:"uid" bson:"uid"`   //UID
	Key         string     `json:"key" bson:"key"`   //KEY
	Data        ss.M       `json:"data" bson:"data"` //数据
	CreateAt    time.Time  `json:"createAt" bson:"createAt"`
	UpdateAt    time.Time  `json:"updateAt" bson:"updateAt"`
	DeleteAt    *time.Time `json:"deleteAt" bson:"deleteAt"`
}

func (this *Ucache) DocName() string { return "Ucache" }
func (this *Ucache) GetId() int64    { return this.Id }
func (this *Ucache) SetId(id int64)  { this.Id = id }
func (this *Ucache) Create() error {
	return this.GetFactory().Create(this)
}
func (this *Ucache) Replace() error {
	return this.GetFactory().Replace(this)
}
func (this *Ucache) Save() error {
	if this.Id == 0 {
		this.CreateAt = time.Now()
		this.UpdateAt = time.Now()
		return this.GetFactory().Create(this)
	} else {
		this.UpdateAt = time.Now()
		return this.GetFactory().Replace(this)
	}
}

// 伪删除
func (this *Ucache) Delete() error {
	return this.GetFactory().Delete(this)
}
func (this *Ucache) UnDelete() error {
	return this.GetFactory().UnDelete(this)
}

// 真删除
func (this *Ucache) Destroy() error {
	return this.GetFactory().Destroy(this)
}
func (this *Ucache) ToString() string {
	return string(this.ToBytes())
}
func (this *Ucache) ToBytes() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *Ucache) Serialize() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *Ucache) UnSerialize(data []byte) []byte {
	ez.Try(json.Unmarshal(data, this))
	return data
}
func (this *Ucache) ClearRelationsBeforeSave() mgo.Doc {
	return this
}
func neverUsed_Ucache() {
	//导入ss包
	a := ss.M{}
	ez.Debug(a)
}

type UcacheAccessControl struct {
	Access  bool
	Message string
	Action  string //控制器Action,小写开头，如c,u,r,d
	Model   *Ucache
	Session *ez.Session
}

func NewUcacheAccessControl(model *Ucache, action string, session *ez.Session) *UcacheAccessControl {
	ctrl := &UcacheAccessControl{
		Access:  true,
		Model:   model,
		Action:  action,
		Session: session,
	}
	ez.DispatchToMany(mc.UcacheAccessControlEvent, ctrl, context.TODO())
	return ctrl
}
