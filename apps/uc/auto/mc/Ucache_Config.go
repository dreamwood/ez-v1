package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	UcacheEventNew           = "uc.UcacheNew"
	UcacheEventBeforeCreate  = "uc.UcacheBeforeCreate"
	UcacheEventBeforeUpdate  = "uc.UcacheBeforeUpdate"
	UcacheEventBeforeSave    = "uc.UcacheBeforeCreate uc.UcacheBeforeUpdate"
	UcacheEventAfterCreate   = "uc.UcacheAfterCreate"
	UcacheEventAfterUpdate   = "uc.UcacheAfterUpdate"
	UcacheEventAfterSave     = "uc.UcacheAfterCreate uc.UcacheAfterUpdate"
	UcacheEventDelete        = "uc.UcacheDelete"
	UcacheAccessControlEvent = "uc.UcacheAccessControl"
)

func GetUcacheConfig() *mgo.DocConfig {
	return Ucache_Config
}

var Ucache_Config *mgo.DocConfig

func init() {
	Ucache_Config = NewUcacheConfig()
}
func NewUcacheConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "uc.Ucache",
		Fields: []string{
			"uid", "key", "data",
		},
		RelationFields:  []string{},
		RelationConfigs: map[string]*mgo.DocRelation{},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
