package env

import (
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/net/tcpsv"
)

var EnvServer *tcpsv.EzTcpServer

func init() {
	conf := GetTcpConfig()
	EnvServer = tcpsv.NewTcpServer(conf.Host, conf.Port)
	EnvServer.OnConnect = func(ip string) {
		ez.LogToConsole("connect from " + ip)
	}
	EnvServer.OnLost = func(ip string) {
		ez.LogToConsole("lost from " + ip)
	}
	EnvServer.OnMessage = func(clientIp string, data []byte) {
		DealWithEnvOnMessage(clientIp, data)
	}
	//go EnvServer.Run()

	//go func() {
	//	for {
	//		time.Sleep(100 * time.Second)
	//		SendPullCommand("192.168.124.51:20108")
	//	}
	//}()
}
