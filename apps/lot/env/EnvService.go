package env

import (
	"fmt"
	"gitee.com/dreamwood/ez-go/ez"
	"strconv"
	"strings"
)

func DealWithEnvOnMessage(ip string, message []byte) {
	dataLen, _ := strconv.ParseInt(fmt.Sprintf("%X", message[2:3]), 16, 64)
	data := Data{}
	for i := 0; i < int(dataLen)/2; i++ {
		v := Bin2Complement(fmt.Sprintf("%X", message[3+i*2:5+i*2]))
		switch i {
		case 0:
			data.CO2 = float64(v)
		case 1:
			data.PM25 = float64(v)
		case 2:
			data.O2 = float64(v)
		case 3:
			data.TVOC = float64(v)
		case 4:
			data.YANWU = float64(v)
		case 5:
			data.WENDU = float64(v)
		case 6:
			data.SHIDU = float64(v)
		case 7:
			data.GUANGZHAO = float64(v)
		case 8:
			data.JIAQUAN = float64(v)
		case 9:
			data.CO = float64(v)
		case 11:
			data.CH4 = float64(v)
		case 12:
			data.ZAOYIN = float64(v)
		case 13:
			data.PM10 = float64(v)
		}
	}
	for _, s := range data.GetString() {
		fmt.Println(s)
	}
}

// 16进制字符串转数据
func Bin2Complement(hexValue string) int64 {
	decimalValue, _ := strconv.ParseInt(hexValue, 16, 64)
	// 将十进制转换为二进制，并计算补码值
	binaryValue := fmt.Sprintf("%016b", decimalValue) // 转换为16位二进制数
	arr := strings.Split(binaryValue, "")
	if arr[0] == "1" {
		for i, s := range arr {
			if s == "0" {
				arr[i] = "1"
			} else {
				arr[i] = "0"
			}
		}
		newBinValue := strings.Join(arr, "")
		decimalValue, _ = strconv.ParseInt(newBinValue, 2, 64)
		decimalValue++
		decimalValue *= -1
	}
	return decimalValue
}

func SendPullCommand(ipAddress string) {
	n, e := EnvServer.Send(ipAddress, []byte{0x01, 0x03, 0x00, 0x00, 0x00, 0x0F, 0x05, 0xCE})
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	println(n)
}
