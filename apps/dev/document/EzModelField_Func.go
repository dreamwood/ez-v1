package dev

import (
	"context"
	"ez/apps/dev/auto/mc"
	"fmt"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"go.mongodb.org/mongo-driver/mongo"
)

func (this *EzModelField) MakeChoice() *ss.M {
	return &ss.M{
		"value": this.Id,
		"label": this.Id,
	}
}

type EzModelFieldCrud struct {
	Factory *mgo.Factory
}

func NewEzModelFieldCrud(args ...interface{}) *EzModelFieldCrud {
	factory := mgo.NewFactory(args...)
	factory.SetDoc(&EzModelField{})
	factory.SetArgus(mc.GetEzModelFieldConfig())
	crud := &EzModelFieldCrud{
		Factory: factory,
	}
	return crud
}
func (this *EzModelFieldCrud) GetCollection() *mongo.Collection {
	return this.Factory.GetCollection()
}
func (this *EzModelFieldCrud) FindId(id int64) (*EzModelField, error) {
	md := new(EzModelField)
	e := this.Factory.FindId(md, id)
	return md, e
}
func (this *EzModelFieldCrud) FindBy(where ss.M, order []string, page int, limit int) ([]*EzModelField, error) {
	list := make([]*EzModelField, 0)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
		Page:       page,
		Limit:      limit,
	}
	e := this.Factory.FindBy(&list, qb)
	return list, e
}
func (this *EzModelFieldCrud) FindOneBy(where ss.M, order []string) (*EzModelField, error) {
	md := new(EzModelField)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
	}
	e := this.Factory.FindOneBy(md, qb)
	return md, e
}
func neverUsed_EzModelField_func() {
	//导入ss包
	a := ss.M{}
	fmt.Printf("%v", a)
	fmt.Printf("%v", context.TODO())
}
