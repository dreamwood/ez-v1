import http from "@/assets/js/http";

export default {
    urlFind: '/dev/admin/EzModel/get',
    urlFindToEdit: '/dev/admin/EzModel/get',
    urlFindBy: '/dev/admin/EzModel/list',
    urlSave: '/dev/admin/EzModel/save',
    //urlDelete: '/dev/admin/EzModel/delete',
    //urlDeleteAll: '/dev/admin/EzModel/delete_many',
    urlDelete: '/dev/admin/EzModel/destroy',
    urlDeleteAll: '/dev/admin/EzModel/destroy_many',
    urlCopy: '/dev/admin/EzModel/copy',
    urlEditMany: '/dev/admin/EzModel/edit_many',
    urlTree: '/dev/admin/EzModel/tree',
    urlChoice: '/dev/admin/EzModel/choice',
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete, {id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}