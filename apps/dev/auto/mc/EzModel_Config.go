package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	EzModelEventNew           = "dev.EzModelNew"
	EzModelEventBeforeCreate  = "dev.EzModelBeforeCreate"
	EzModelEventBeforeUpdate  = "dev.EzModelBeforeUpdate"
	EzModelEventBeforeSave    = "dev.EzModelBeforeCreate dev.EzModelBeforeUpdate"
	EzModelEventAfterCreate   = "dev.EzModelAfterCreate"
	EzModelEventAfterUpdate   = "dev.EzModelAfterUpdate"
	EzModelEventAfterSave     = "dev.EzModelAfterCreate dev.EzModelAfterUpdate"
	EzModelEventDelete        = "dev.EzModelDelete"
	EzModelAccessControlEvent = "dev.EzModelAccessControl"
)

func GetEzModelConfig() *mgo.DocConfig {
	return EzModel_Config
}

var EzModel_Config *mgo.DocConfig

func init() {
	EzModel_Config = NewEzModelConfig()
}
func NewEzModelConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "dev.EzModel",
		Fields: []string{
			"app", "dir", "name", "cnName", "fields",
		},
		RelationFields: []string{
			"fields",
		},
		RelationConfigs: map[string]*mgo.DocRelation{
			"fields": {
				Config:     GetEzModelFieldConfig,
				DocName:    "EzModelField",
				JoinType:   "M",
				KeyInside:  "id",
				KeyOutSide: "modelId",
			},
		},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
