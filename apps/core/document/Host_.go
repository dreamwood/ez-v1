package core

import (
	"context"
	"encoding/json"
	"ez/apps/core/auto/mc"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"time"
)

type Host struct {
	mgo.BaseDoc `bson:"-" json:"-"`
	Id          int64      `json:"id" bson:"id,omitempty"`
	AppId       string     `json:"appId" bson:"appId"`             //APPID
	MachineCode string     `json:"machineCode" bson:"machineCode"` //机器码
	Ip          string     `json:"ip" bson:"ip"`                   //IP地址
	Port        int64      `json:"port" bson:"port"`               //端口
	Weight      int64      `json:"weight" bson:"weight"`           //权重
	IsOn        bool       `json:"isOn" bson:"isOn"`               //启用
	State       int64      `json:"state" bson:"state"`             //状态码
	StateInfo   string     `json:"stateInfo" bson:"stateInfo"`     //状态信息
	LastTime    time.Time  `json:"lastTime" bson:"lastTime"`       //上次心跳时间
	CreateAt    time.Time  `json:"createAt" bson:"createAt"`
	UpdateAt    time.Time  `json:"updateAt" bson:"updateAt"`
	DeleteAt    *time.Time `json:"deleteAt" bson:"deleteAt"`
}

func (this *Host) DocName() string { return "Host" }
func (this *Host) GetId() int64    { return this.Id }
func (this *Host) SetId(id int64)  { this.Id = id }
func (this *Host) Create() error {
	return this.GetFactory().Create(this)
}
func (this *Host) Replace() error {
	return this.GetFactory().Replace(this)
}
func (this *Host) Save() error {
	if this.Id == 0 {
		this.CreateAt = time.Now()
		this.UpdateAt = time.Now()
		return this.GetFactory().Create(this)
	} else {
		this.UpdateAt = time.Now()
		return this.GetFactory().Replace(this)
	}
}

// 伪删除
func (this *Host) Delete() error {
	return this.GetFactory().Delete(this)
}
func (this *Host) UnDelete() error {
	return this.GetFactory().UnDelete(this)
}

// 真删除
func (this *Host) Destroy() error {
	return this.GetFactory().Destroy(this)
}
func (this *Host) ToString() string {
	return string(this.ToBytes())
}
func (this *Host) ToBytes() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *Host) Serialize() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *Host) UnSerialize(data []byte) []byte {
	ez.Try(json.Unmarshal(data, this))
	return data
}
func (this *Host) ClearRelationsBeforeSave() mgo.Doc {
	return this
}
func neverUsed_Host() {
	//导入ss包
	a := ss.M{}
	ez.Debug(a)
}

type HostAccessControl struct {
	Access  bool
	Message string
	Action  string //控制器Action,小写开头，如c,u,r,d
	Model   *Host
	Session *ez.Session
}

func NewHostAccessControl(model *Host, action string, session *ez.Session) *HostAccessControl {
	ctrl := &HostAccessControl{
		Access:  true,
		Model:   model,
		Action:  action,
		Session: session,
	}
	ez.DispatchToMany(mc.HostAccessControlEvent, ctrl, context.TODO())
	return ctrl
}
