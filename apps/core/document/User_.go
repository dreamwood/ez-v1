package core

import (
	"context"
	"encoding/json"
	"ez/apps/core/auto/mc"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"time"
)

type User struct {
	mgo.BaseDoc `bson:"-" json:"-"`
	Id          int64      `json:"id" bson:"id,omitempty"`
	Account     string     `json:"account" bson:"account"`   //账号
	Password    string     `json:"password" bson:"password"` //密码
	Salt        string     `json:"salt" bson:"salt"`         //Salt
	Token       string     `json:"token" bson:"token"`       //Token
	Pic         string     `json:"pic" bson:"pic"`           //头像
	Name        string     `json:"name" bson:"name"`         //姓名
	Code        string     `json:"code" bson:"code"`         //工号
	Phone       string     `json:"phone" bson:"phone"`       //手机号
	IdCard      string     `json:"idCard" bson:"idCard"`     //身份证
	Sex         int64      `json:"sex" bson:"sex"`           //性别
	Age         int64      `json:"age" bson:"age"`           //年龄
	Roles       []*Role    `json:"roles" bson:"roles"`       //角色
	RolesIds    []int64    `json:"rolesIds" bson:"rolesIds"` //角色
	CreateAt    time.Time  `json:"createAt" bson:"createAt"`
	UpdateAt    time.Time  `json:"updateAt" bson:"updateAt"`
	DeleteAt    *time.Time `json:"deleteAt" bson:"deleteAt"`
}

func (this *User) DocName() string { return "User" }
func (this *User) GetId() int64    { return this.Id }
func (this *User) SetId(id int64)  { this.Id = id }
func (this *User) Create() error {
	return this.GetFactory().Create(this)
}
func (this *User) Replace() error {
	return this.GetFactory().Replace(this)
}
func (this *User) Save() error {
	if this.Id == 0 {
		this.CreateAt = time.Now()
		this.UpdateAt = time.Now()
		return this.GetFactory().Create(this)
	} else {
		this.UpdateAt = time.Now()
		return this.GetFactory().Replace(this)
	}
}

// 伪删除
func (this *User) Delete() error {
	return this.GetFactory().Delete(this)
}
func (this *User) UnDelete() error {
	return this.GetFactory().UnDelete(this)
}

// 真删除
func (this *User) Destroy() error {
	return this.GetFactory().Destroy(this)
}
func (this *User) ToString() string {
	return string(this.ToBytes())
}
func (this *User) ToBytes() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *User) Serialize() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *User) UnSerialize(data []byte) []byte {
	ez.Try(json.Unmarshal(data, this))
	return data
}
func (this *User) LoadRoles() {
	if this.Roles == nil {
		this.Roles = make([]*Role, 0)
	}
	if this.RolesIds == nil {
		this.RolesIds = make([]int64, 0)
	}
	if len(this.RolesIds) > 0 {
		this.Roles = make([]*Role, 0)
		for _, v := range this.RolesIds {
			find, e := NewRoleCrud().FindId(v)
			if e == nil {
				this.Roles = append(this.Roles, find)
			}
		}
	}
}
func (this *User) ClearRelationsBeforeSave() mgo.Doc {
	this.Roles = nil
	return this
}
func neverUsed_User() {
	//导入ss包
	a := ss.M{}
	ez.Debug(a)
}

type UserAccessControl struct {
	Access  bool
	Message string
	Action  string //控制器Action,小写开头，如c,u,r,d
	Model   *User
	Session *ez.Session
}

func NewUserAccessControl(model *User, action string, session *ez.Session) *UserAccessControl {
	ctrl := &UserAccessControl{
		Access:  true,
		Model:   model,
		Action:  action,
		Session: session,
	}
	ez.DispatchToMany(mc.UserAccessControlEvent, ctrl, context.TODO())
	return ctrl
}
