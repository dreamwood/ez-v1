package core

import (
	"context"
	"ez/apps/core/auto/mc"
	"fmt"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"go.mongodb.org/mongo-driver/mongo"
)

func (this *Host) MakeChoice() *ss.M {
	return &ss.M{"value": this.Id, "label": this.Id}
}

type HostCrud struct {
	Factory *mgo.Factory
}

func NewHostCrud(args ...interface{}) *HostCrud {
	factory := mgo.NewFactory(args...)
	factory.SetDoc(&Host{})
	factory.SetArgus(mc.GetHostConfig())
	crud := &HostCrud{
		Factory: factory,
	}
	return crud
}
func (this *HostCrud) GetCollection() *mongo.Collection {
	return this.Factory.GetCollection()
}
func (this *HostCrud) FindId(id int64) (*Host, error) {
	md := new(Host)
	e := this.Factory.FindId(md, id)
	return md, e
}
func (this *HostCrud) FindBy(where ss.M, order []string, page int, limit int) ([]*Host, error) {
	list := make([]*Host, 0)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
		Page:       page,
		Limit:      limit,
	}
	e := this.Factory.FindBy(&list, qb)
	return list, e
}
func (this *HostCrud) FindOneBy(where ss.M, order []string) (*Host, error) {
	md := new(Host)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
	}
	e := this.Factory.FindOneBy(md, qb)
	return md, e
}
func neverUsed_Host_func() {
	//导入ss包
	a := ss.M{}
	fmt.Printf("%v", a)
	fmt.Printf("%v", context.TODO())
}
