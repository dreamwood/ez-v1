package core

import (
	"context"
	"encoding/json"
	"ez/apps/core/auto/mc"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"time"
)

type Model struct {
	mgo.BaseDoc `bson:"-" json:"-"`
	Id          int64      `json:"id" bson:"id,omitempty"`
	AppId       string     `json:"appId" bson:"appId"`   //APPID
	Name        string     `json:"name" bson:"name"`     //模块名
	CnName      string     `json:"cnName" bson:"cnName"` //中文名
	Entry       string     `json:"entry" bson:"entry"`   //入口地址
	IsOn        bool       `json:"isOn" bson:"isOn"`     //开启
	CreateAt    time.Time  `json:"createAt" bson:"createAt"`
	UpdateAt    time.Time  `json:"updateAt" bson:"updateAt"`
	DeleteAt    *time.Time `json:"deleteAt" bson:"deleteAt"`
}

func (this *Model) DocName() string { return "Model" }
func (this *Model) GetId() int64    { return this.Id }
func (this *Model) SetId(id int64)  { this.Id = id }
func (this *Model) Create() error {
	return this.GetFactory().Create(this)
}
func (this *Model) Replace() error {
	return this.GetFactory().Replace(this)
}
func (this *Model) Save() error {
	if this.Id == 0 {
		this.CreateAt = time.Now()
		this.UpdateAt = time.Now()
		return this.GetFactory().Create(this)
	} else {
		this.UpdateAt = time.Now()
		return this.GetFactory().Replace(this)
	}
}

// 伪删除
func (this *Model) Delete() error {
	return this.GetFactory().Delete(this)
}
func (this *Model) UnDelete() error {
	return this.GetFactory().UnDelete(this)
}

// 真删除
func (this *Model) Destroy() error {
	return this.GetFactory().Destroy(this)
}
func (this *Model) ToString() string {
	return string(this.ToBytes())
}
func (this *Model) ToBytes() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *Model) Serialize() []byte {
	data, e := json.Marshal(this)
	ez.Try(e)
	return data
}
func (this *Model) UnSerialize(data []byte) []byte {
	ez.Try(json.Unmarshal(data, this))
	return data
}
func (this *Model) ClearRelationsBeforeSave() mgo.Doc {
	return this
}
func neverUsed_Model() {
	//导入ss包
	a := ss.M{}
	ez.Debug(a)
}

type ModelAccessControl struct {
	Access  bool
	Message string
	Action  string //控制器Action,小写开头，如c,u,r,d
	Model   *Model
	Session *ez.Session
}

func NewModelAccessControl(model *Model, action string, session *ez.Session) *ModelAccessControl {
	ctrl := &ModelAccessControl{
		Access:  true,
		Model:   model,
		Action:  action,
		Session: session,
	}
	ez.DispatchToMany(mc.ModelAccessControlEvent, ctrl, context.TODO())
	return ctrl
}
