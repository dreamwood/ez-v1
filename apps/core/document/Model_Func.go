package core

import (
	"context"
	"ez/apps/core/auto/mc"
	"fmt"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"go.mongodb.org/mongo-driver/mongo"
)

func (this *Model) MakeChoice() *ss.M {
	return &ss.M{"value": this.Id, "label": fmt.Sprintf("[ %s ] %s", this.Name, this.CnName)}
}

type ModelCrud struct {
	Factory *mgo.Factory
}

func NewModelCrud(args ...interface{}) *ModelCrud {
	factory := mgo.NewFactory(args...)
	factory.SetDoc(&Model{})
	factory.SetArgus(mc.GetModelConfig())
	crud := &ModelCrud{
		Factory: factory,
	}
	return crud
}
func (this *ModelCrud) GetCollection() *mongo.Collection {
	return this.Factory.GetCollection()
}
func (this *ModelCrud) FindId(id int64) (*Model, error) {
	md := new(Model)
	e := this.Factory.FindId(md, id)
	return md, e
}
func (this *ModelCrud) FindBy(where ss.M, order []string, page int, limit int) ([]*Model, error) {
	list := make([]*Model, 0)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
		Page:       page,
		Limit:      limit,
	}
	e := this.Factory.FindBy(&list, qb)
	return list, e
}
func (this *ModelCrud) FindOneBy(where ss.M, order []string) (*Model, error) {
	md := new(Model)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
	}
	e := this.Factory.FindOneBy(md, qb)
	return md, e
}
func neverUsed_Model_func() {
	//导入ss包
	a := ss.M{}
	fmt.Printf("%v", a)
	fmt.Printf("%v", context.TODO())
}
