package core

import (
	"context"
	"ez/apps/core/auto/mc"
	"fmt"
	"gitee.com/dreamwood/ez-go/db/mgo"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"go.mongodb.org/mongo-driver/mongo"
)

func (this *RequestLog) MakeChoice() *ss.M {
	return &ss.M{
		"value": this.Id,
		"label": this.Id,
	}
}

type RequestLogCrud struct {
	Factory *mgo.Factory
}

func NewRequestLogCrud(args ...interface{}) *RequestLogCrud {
	factory := mgo.NewFactory(args...)
	factory.SetDoc(&RequestLog{})
	factory.SetArgus(mc.GetRequestLogConfig())
	crud := &RequestLogCrud{
		Factory: factory,
	}
	return crud
}
func (this *RequestLogCrud) GetCollection() *mongo.Collection {
	return this.Factory.GetCollection()
}
func (this *RequestLogCrud) FindId(id int64) (*RequestLog, error) {
	md := new(RequestLog)
	e := this.Factory.FindId(md, id)
	return md, e
}
func (this *RequestLogCrud) FindBy(where ss.M, order []string, page int, limit int) ([]*RequestLog, error) {
	list := make([]*RequestLog, 0)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
		Page:       page,
		Limit:      limit,
	}
	e := this.Factory.FindBy(&list, qb)
	return list, e
}
func (this *RequestLogCrud) FindOneBy(where ss.M, order []string) (*RequestLog, error) {
	md := new(RequestLog)
	qb := &ez.HttpQuery{
		Conditions: where,
		Order:      order,
	}
	e := this.Factory.FindOneBy(md, qb)
	return md, e
}
func neverUsed_RequestLog_func() {
	//导入ss包
	a := ss.M{}
	fmt.Printf("%v", a)
	fmt.Printf("%v", context.TODO())
}
