package task

import (
	"ez/apps/core/service"
	"gitee.com/dreamwood/ez-go/task"
)

func init() {
	task.NewTask("HeartBeatCheck", func() {
		service.CheckHostAlive()
	}).SetEvery(10)
}
