package gen

import "gitee.com/dreamwood/ez-go/maker"

func RequestLog() {
	doc := maker.CreateDoc("RequestLog", "请求日志", "core")
	doc.Add("url", "").IsString()
	doc.Add("session", "").IsString()
	doc.Add("logs", "").IsAny("[]*ez.SessionLog")
	doc.Generate()
}
