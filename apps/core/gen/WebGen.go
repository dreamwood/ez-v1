package gen

import "gitee.com/dreamwood/ez-go/maker"

func Model() {
	doc := maker.CreateDoc("Model", "模块", "core")
	doc.Add("appId", "APPID").IsString()
	doc.Add("name", "模块名").IsString()
	doc.Add("cnName", "中文名").IsString()
	doc.Add("entry", "入口地址").IsString()
	doc.Add("isOn", "开启").IsBool()
	doc.Generate()
}
func Top() {
	doc := maker.CreateDoc("Top", "顶部菜单", "core")
	doc.Add("model", "所属模块").IsJoinM2O("Model")
	doc.Add("name", "名称").IsString()
	doc.Add("url", "URL").IsString()
	doc.Add("icon", "图标").IsString()
	doc.Add("sort", "排序").IsInt()
	doc.Add("l", "L").IsInt()
	doc.Add("r", "R").IsInt()
	doc.Add("level", "Level").IsInt()
	doc.Add("link", "link").IsString()
	doc.Add("parent", "父级菜单").IsJoinM2O("Top")
	doc.Add("children", "子菜单").IsJoinO2M("Top", "id", "ParentId")

	doc.IsTree = true

	doc.Generate()
}

func Menu() {
	doc := maker.CreateDoc("Menu", "左侧菜单", "core")
	doc.Add("model", "所属模块").IsJoinM2O("Model")
	doc.Add("name", "名称").IsString()
	doc.Add("url", "URL").IsString()
	doc.Add("icon", "图标").IsString()
	doc.Add("sort", "排序").IsInt()
	doc.Add("l", "L").IsInt()
	doc.Add("r", "R").IsInt()
	doc.Add("level", "Level").IsInt()
	doc.Add("link", "link").IsString()
	doc.Add("parent", "父级菜单").IsJoinM2O("Menu")
	doc.Add("children", "子菜单").IsJoinO2M("Menu", "id", "ParentId")

	doc.IsTree = true

	doc.Generate()
}

func Api() {
	doc := maker.CreateDoc("Api", "接口", "core")
	doc.Add("route", "路由").IsString()
	doc.Add("name", "名称").IsString()
	doc.Add("app", "应用").IsString()
	doc.Add("isPublic", "是否公开").IsBool()
	doc.LoadTree()
	//角色的数据那边维护了一套接口权限，接口这边根据角色的数据来重新生成接口权限
	doc.Add("rolesAllow", "允许角色").IsJoinM2M("Role")
	doc.Add("rolesDeny", "允许角色").IsJoinM2M("Role")
	doc.Generate()
}

func Page() {
	doc := maker.CreateDoc("Page", "页面权限", "core")
	doc.Add("app", "应用").IsString()
	doc.Add("name", "名称").IsString()
	doc.Add("key", "KEY").IsString()
	doc.Add("route", "前端页面").IsString()
	doc.Add("db", "数据模型").IsString()
	doc.Generate()
}
