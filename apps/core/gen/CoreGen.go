package gen

import "gitee.com/dreamwood/ez-go/maker"

func init() {

}

func GateWay() {
	doc := maker.CreateDoc("GateWay", "网关", "core")
	doc.Add("Ip", "IP地址").IsString()
	doc.Add("Port", "端口").IsInt()
	doc.Add("IsOn", "开启").IsBool()
	doc.Add("Hosts", "主机").IsJoinM2M("Host")
	doc.Generate()
}

func Host() {
	doc := maker.CreateDoc("Host", "主机", "core")
	doc.Add("appId", "APPID").IsString()
	doc.Add("machineCode", "机器码").IsString()
	doc.Add("ip", "IP地址").IsString()
	doc.Add("port", "端口").IsInt()
	doc.Add("weight", "权重").IsInt()
	doc.Add("isOn", "启用").IsBool()
	doc.Add("state", "状态码").IsInt()
	doc.Add("stateInfo", "状态信息").IsString()
	doc.Add("lastTime", "上次心跳时间").IsTime()
	doc.Generate()
}

func Route() {
	doc := maker.CreateDoc("Route", "路由", "core")
	doc.Add("AppId", "APPID").IsString()
	doc.Add("From", "From").IsString()
	doc.Add("To", "To").IsString()
	doc.Add("IsOn", "启用").IsBool()
	doc.Add("Sort", "权重").IsInt()
	doc.Add("GateWay", "所属网关").IsJoinM2O("GateWay")
	doc.Generate()
}
