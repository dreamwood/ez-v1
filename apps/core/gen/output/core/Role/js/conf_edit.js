import formBuilder from "@/comps/form/formBuilder";
import api from "./api";
/*
fb.addText("name","角色名",3)
fb.addText("code","编码",3)
fb.addText("model","可用模块",3)
fb.addText("top","顶栏权限",3)
fb.addText("menu","菜单权限",3)
fb.addText("apiAllow","放通接口权限",3)
fb.addText("apiDeny","禁止接口权限",3)
fb.addText("apiAllowKeys","禁止接口权限",3)
fb.addText("apiDenyKeys","禁止接口权限",3)
fb.addText("page","页面权限",3)
fb.addText("pageAllowKeys","页面放行权限",3)
fb.addText("pageDenyKeys","页面禁止权限",3)

* */
export default {
    main:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)

        fb.addText("name").setSpan(3).setLabel("姓名")

        fb.addText("sort").setSpan(3).setLabel("排序").setTypeNumber()

        return fb
    },
    tree:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)

        fb.addCascade("parentId").setLabel("上级分类").setSpan(3)
            .setApi(api.urlTree)

        fb.addText("name").setSpan(6).setLabel("分类名称")
            .setRules([fb.R.notNull("名称不能为空")])

        fb.addText("sort").setSpan(3).setLabel("排序").setTypeNumber()
        return fb
    },
    other:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100).setLabelPosition("right")

        return fb
    }
}