import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("name","角色名",80)
lb.add("code","编码",80)
lb.add("model","可用模块",80)
lb.add("top","顶栏权限",80)
lb.add("menu","菜单权限",80)
lb.add("apiAllow","放通接口权限",80)
lb.add("apiDeny","禁止接口权限",80)
lb.add("apiAllowKeys","禁止接口权限",80)
lb.add("apiDenyKeys","禁止接口权限",80)
lb.add("page","页面权限",80)
lb.add("pageAllowKeys","页面放行权限",80)
lb.add("pageDenyKeys","页面禁止权限",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.addAction(120).setAlignRight()
    return lb.headers
}