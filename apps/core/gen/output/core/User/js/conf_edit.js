import formBuilder from "@/comps/form/formBuilder";
import api from "./api";
/*
fb.addText("account","账号",3)
fb.addText("password","密码",3)
fb.addText("salt","Salt",3)
fb.addText("token","Token",3)
fb.addText("pic","头像",3)
fb.addText("name","姓名",3)
fb.addText("code","工号",3)
fb.addText("phone","手机号",3)
fb.addText("idCard","身份证",3)
fb.addText("sex","性别",3)
fb.addText("age","年龄",3)
fb.addText("roles","角色",3)

* */
export default {
    main:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)

        fb.addText("name").setSpan(3).setLabel("姓名")

        fb.addText("sort").setSpan(3).setLabel("排序").setTypeNumber()

        return fb
    },
    tree:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)

        fb.addCascade("parentId").setLabel("上级分类").setSpan(3)
            .setApi(api.urlTree)

        fb.addText("name").setSpan(6).setLabel("分类名称")
            .setRules([fb.R.notNull("名称不能为空")])

        fb.addText("sort").setSpan(3).setLabel("排序").setTypeNumber()
        return fb
    },
    other:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100).setLabelPosition("right")

        return fb
    }
}