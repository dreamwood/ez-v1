import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("route","路由",80)
lb.add("name","名称",80)
lb.add("app","应用",80)
lb.add("isPublic","是否公开",80)
lb.add("sort","排序",80)
lb.add("l","L",80)
lb.add("r","R",80)
lb.add("level","Level",80)
lb.add("link","link",80)
lb.add("parent","父级菜单",80)
lb.add("children","子菜单",80)
lb.add("rolesAllow","允许角色",80)
lb.add("rolesDeny","允许角色",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.addAction(120).setAlignRight()
    return lb.headers
}