package controller

import (
	"ez/apps/core/auto/controller"
	core "ez/apps/core/document"
	"ez/apps/core/service"
	"ez/config/r"
	"ez/custom/cc"
	"gitee.com/dreamwood/ez-go/ez"
)

type UserController struct {
	controller.UserAutoController
}

func init() {
	c := &UserController{}
	ez.CreateApi(r.AdminGet("User"), c.GetAction)
	ez.CreateApi(r.AdminList("User"), c.ListAction)
	ez.CreateApi(r.AdminSave("User"), c.SaveAction)
	ez.CreateApi(r.AdminCopy("User"), c.CopyAction)
	ez.CreateApi(r.AdminUpdate("User"), c.UpdateAction)
	ez.CreateApi(r.AdminChoice("User"), c.ChoiceAction)
	ez.CreateApi(r.AdminDelete("User"), c.DeleteAction)
	ez.CreateApi(r.AdminUnDelete("User"), c.UnDeleteAction)
	ez.CreateApi(r.AdminDestroy("User"), c.DestroyAction)
	ez.CreateApi(r.AdminUpdateMany("User"), c.UpdateManyAction)
	ez.CreateApi(r.AdminDeleteMany("User"), c.DeleteManyAction)
	ez.CreateApi(r.AdminDestroyMany("User"), c.DestroyManyAction)
	ez.CreateApi(r.AdminApi("/User/reset_password"), c.ResetPasswordAction)
}

// func (c UserController) AccessControl(session *ez.Session) { /* 在这里面重构 */ }
// func (c UserController) GetAction(session *ez.Session) { /* 在这里面重构 */ }
// func (c UserController) ListAction(session *ez.Session) { /* 在这里面重构 */ }
func (c *UserController) ResetPasswordAction(session *ez.Session) {
	this := cc.New(session)
	id := this.Get("id").IsInt64()
	crud := core.NewUserCrud()
	user, e := crud.FindId(id)
	if this.Try(e) {
		return
	}
	user.Password = service.GenPass("123123", user.Salt)
	user.Save()
	this.ReturnSuccess("密码已重置为：123123", "")
}
