package controller

import (
	"ez/config/r"
	"ez/custom/cc"
	"gitee.com/dreamwood/ez-go/ez"
)

type TestController struct {
}

func init() {
	c := new(TestController)
	ez.CreateApi(r.Api("/test"), c.TestAction)
}

func (c TestController) TestAction(session *ez.Session) {
	this := cc.New(session)
	for r, _ := range ez.GetRouteHub().DirectRouter {
		ez.LogToConsole(r)
	}
	for r, _ := range ez.GetRouteHub().RegxRouter {
		ez.LogToConsole(r)
	}
	this.ReturnSuccess("OK", "")
}
