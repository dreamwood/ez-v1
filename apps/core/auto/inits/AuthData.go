package inits

import (
	core "ez/apps/core/document"
	"ez/apps/core/service"
	"gitee.com/dreamwood/ez-go/ez"
)

func Role() {
	doc := new(core.Role)
	doc.Id = 1
	doc.Name = "超级管理员"
	doc.Code = ez.Role_Super_Admin
	doc.Create()

	doc1 := new(core.Role)
	doc.Id = 2
	doc1.Name = "管理员"
	doc1.Code = ez.Role_Admin
	doc1.Create()
}

func User() {
	U := new(core.User)
	U.Id = 1
	U.Account = "sa"
	U.Salt = "ABC"
	U.Password = service.GenPass("123123", "ABC")
	U.RolesIds = []int64{1, 2}
	U.Create()
}

func Model() {
	M := new(core.Model)
	M.Id = 1
	M.Name = "EzCore"
	M.Entry = ""
	M.Create()
}

func GateWay() {
	M := new(core.GateWay)
	M.Id = 1
	M.Ip = "0.0.0.0"
	M.Port = 8080
	M.IsOn = true
	M.Create()
}
