package db

import (
	"context"
	"encoding/json"
	core "ez/apps/core/document"
	"fmt"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"gitee.com/dreamwood/ez-go/tools"
	"go.mongodb.org/mongo-driver/bson"
	"os"
	"testing"
)

func DropGateWay() {
	ez.DBMongo.Collection("GateWay").Drop(context.TODO())
}
func TestDropGateWay(t *testing.T) {
	ez.DefaultInit()
	DropGateWay()
}
func BackUpGateWay(targetFile string) {
	if targetFile == "" {
		targetFile = "./GateWay.json"
	}
	ez.DefaultInit()
	//计数
	total, e := ez.DBMongo.Collection("GateWay").CountDocuments(context.TODO(), bson.M{})
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	f, e := os.OpenFile(targetFile, os.O_CREATE|os.O_WRONLY|os.O_RDWR, 0666)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	defer f.Close()
	crud := core.NewGateWayCrud()
	data := make([]core.GateWay, 0)
	for i := 0; i <= int(total/1000); i++ {
		rows, e := crud.FindBy(ss.M{}, nil, i, 1000)
		if e != nil {
			ez.LogToConsole(e.Error())
		}
		for _, row := range rows {
			data = append(data, *row)
		}
	}
	content, e := json.Marshal(data)
	f.Write(content)
}
func TestBackUpGateWay(t *testing.T) {
	ez.DefaultInit()
	targetFile := fmt.Sprintf("./GateWay_%s.json", tools.GetDateYMDHIS("", "", "_"))
	BackUpGateWay(targetFile)
}
func RecoverGateWay(filePath string) {
	if filePath == "" {
		filePath = "./GateWay.json"
	}
	ez.DefaultInit()
	ez.DBMongo.Collection("GateWay").Drop(context.TODO())
	content := tools.ReadFile(filePath)
	data := make([]core.GateWay, 0)
	e := json.Unmarshal(content, &data)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	for _, row := range data {
		e = row.Create()
		if e != nil {
			ez.LogToConsole(e.Error())
		}
	}
	ez.LogToConsole(fmt.Sprintf("写入数据%d条", len(data)))
}
func TestRecoverGateWay(t *testing.T) {
	filePath := "./GateWay_20240107_190550.json"
	ez.DefaultInit()
	RecoverGateWay(filePath)
}
