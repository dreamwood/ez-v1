package db

import (
	"context"
	"encoding/json"
	core "ez/apps/core/document"
	"fmt"
	"gitee.com/dreamwood/ez-go/ez"
	"gitee.com/dreamwood/ez-go/ss"
	"gitee.com/dreamwood/ez-go/tools"
	"go.mongodb.org/mongo-driver/bson"
	"os"
	"testing"
)

func DropModel() {
	ez.DBMongo.Collection("Model").Drop(context.TODO())
}
func TestDropModel(t *testing.T) {
	ez.DefaultInit()
	DropModel()
}
func BackUpModel(targetFile string) {
	if targetFile == "" {
		targetFile = "./Model.json"
	}
	ez.DefaultInit()
	//计数
	total, e := ez.DBMongo.Collection("Model").CountDocuments(context.TODO(), bson.M{})
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	f, e := os.OpenFile(targetFile, os.O_CREATE|os.O_WRONLY|os.O_RDWR, 0666)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	defer f.Close()
	crud := core.NewModelCrud()
	data := make([]core.Model, 0)
	for i := 0; i <= int(total/1000); i++ {
		rows, e := crud.FindBy(ss.M{}, nil, i, 1000)
		if e != nil {
			ez.LogToConsole(e.Error())
		}
		for _, row := range rows {
			data = append(data, *row)
		}
	}
	content, e := json.Marshal(data)
	f.Write(content)
}
func TestBackUpModel(t *testing.T) {
	ez.DefaultInit()
	targetFile := fmt.Sprintf("./Model_%s.json", tools.GetDateYMDHIS("", "", "_"))
	BackUpModel(targetFile)
}
func RecoverModel(filePath string) {
	if filePath == "" {
		filePath = "./Model.json"
	}
	ez.DefaultInit()
	ez.DBMongo.Collection("Model").Drop(context.TODO())
	content := tools.ReadFile(filePath)
	data := make([]core.Model, 0)
	e := json.Unmarshal(content, &data)
	if e != nil {
		ez.LogToConsole(e.Error())
	}
	for _, row := range data {
		e = row.Create()
		if e != nil {
			ez.LogToConsole(e.Error())
		}
	}
	ez.LogToConsole(fmt.Sprintf("写入数据%d条", len(data)))
}
func TestRecoverModel(t *testing.T) {
	filePath := "./Model_20240107_190550.json"
	ez.DefaultInit()
	RecoverModel(filePath)
}
