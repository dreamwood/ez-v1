package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	RequestLogEventNew           = "core.RequestLogNew"
	RequestLogEventBeforeCreate  = "core.RequestLogBeforeCreate"
	RequestLogEventBeforeUpdate  = "core.RequestLogBeforeUpdate"
	RequestLogEventBeforeSave    = "core.RequestLogBeforeCreate core.RequestLogBeforeUpdate"
	RequestLogEventAfterCreate   = "core.RequestLogAfterCreate"
	RequestLogEventAfterUpdate   = "core.RequestLogAfterUpdate"
	RequestLogEventAfterSave     = "core.RequestLogAfterCreate core.RequestLogAfterUpdate"
	RequestLogEventDelete        = "core.RequestLogDelete"
	RequestLogAccessControlEvent = "core.RequestLogAccessControl"
)

func GetRequestLogConfig() *mgo.DocConfig {
	return RequestLog_Config
}

var RequestLog_Config *mgo.DocConfig

func init() {
	RequestLog_Config = NewRequestLogConfig()
}
func NewRequestLogConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "core.RequestLog",
		Fields: []string{
			"url", "session", "logs",
		},
		RelationFields:  []string{},
		RelationConfigs: map[string]*mgo.DocRelation{},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
