package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	ModelEventNew           = "core.ModelNew"
	ModelEventBeforeCreate  = "core.ModelBeforeCreate"
	ModelEventBeforeUpdate  = "core.ModelBeforeUpdate"
	ModelEventBeforeSave    = "core.ModelBeforeCreate core.ModelBeforeUpdate"
	ModelEventAfterCreate   = "core.ModelAfterCreate"
	ModelEventAfterUpdate   = "core.ModelAfterUpdate"
	ModelEventAfterSave     = "core.ModelAfterCreate core.ModelAfterUpdate"
	ModelEventDelete        = "core.ModelDelete"
	ModelAccessControlEvent = "core.ModelAccessControl"
)

func GetModelConfig() *mgo.DocConfig {
	return Model_Config
}

var Model_Config *mgo.DocConfig

func init() {
	Model_Config = NewModelConfig()
}
func NewModelConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "core.Model",
		Fields: []string{
			"appId", "name", "cnName", "entry", "isOn",
		},
		RelationFields:  []string{},
		RelationConfigs: map[string]*mgo.DocRelation{},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
