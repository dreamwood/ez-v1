package mc

import (
	"gitee.com/dreamwood/ez-go/db/mgo"
)

const (
	ApiEventNew           = "core.ApiNew"
	ApiEventBeforeCreate  = "core.ApiBeforeCreate"
	ApiEventBeforeUpdate  = "core.ApiBeforeUpdate"
	ApiEventBeforeSave    = "core.ApiBeforeCreate core.ApiBeforeUpdate"
	ApiEventAfterCreate   = "core.ApiAfterCreate"
	ApiEventAfterUpdate   = "core.ApiAfterUpdate"
	ApiEventAfterSave     = "core.ApiAfterCreate core.ApiAfterUpdate"
	ApiEventDelete        = "core.ApiDelete"
	ApiAccessControlEvent = "core.ApiAccessControl"
)

func GetApiConfig() *mgo.DocConfig {
	return Api_Config
}

var Api_Config *mgo.DocConfig

func init() {
	Api_Config = NewApiConfig()
}
func NewApiConfig() *mgo.DocConfig {
	return &mgo.DocConfig{
		ContainerKey: "core.Api",
		Fields: []string{
			"route", "name", "app", "isPublic", "sort", "l", "r", "level", "link", "parent", "children", "rolesAllow", "rolesDeny",
		},
		RelationFields: []string{
			"parent", "children", "rolesAllow", "rolesDeny",
		},
		RelationConfigs: map[string]*mgo.DocRelation{
			"parent": {
				Config:     GetApiConfig,
				DocName:    "Api",
				JoinType:   "O",
				KeyInside:  "parentId",
				KeyOutSide: "id",
			},
			"children": {
				Config:     GetApiConfig,
				DocName:    "Api",
				JoinType:   "M",
				KeyInside:  "id",
				KeyOutSide: "parentId",
			},
			"rolesAllow": {
				Config:     GetRoleConfig,
				DocName:    "Role",
				JoinType:   "MM",
				KeyInside:  "rolesAllowIds",
				KeyOutSide: "id",
			},
			"rolesDeny": {
				Config:     GetRoleConfig,
				DocName:    "Role",
				JoinType:   "MM",
				KeyInside:  "rolesDenyIds",
				KeyOutSide: "id",
			},
		},
		FieldFilter: map[string]*mgo.DocFieldFilter{
			"default": {
				Select: []string{},
				Omit:   []string{},
			},
		},
	}
}
