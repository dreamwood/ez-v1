package event

import (
	"context"
	"ez/apps/core/auto/mc"
	"ez/apps/core/service"
	"gitee.com/dreamwood/ez-go/ez"
)

func init() {
	ez.Subscribe(mc.RoleEventBeforeSave, func(v interface{}, ctx context.Context) {
		go service.CreateApiRoles()
	})
	ez.Subscribe(mc.RoleEventDelete, func(v interface{}, ctx context.Context) {
		go service.CreateApiRoles()
	})
}
