package gateway

import (
	"ez/config/r"
	"gitee.com/dreamwood/ez-go/ez"
)

func init() {

	ez.CreateApi(r.R("/_server_500"), func(session *ez.Session) {
		code := session.Get("code").IsString()
		ez.Debug(code)
		session.Html("500")
	})
}
