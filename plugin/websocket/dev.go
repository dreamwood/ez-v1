package ws

import "fmt"

func (hub *Hub) debugSession() {
	if hub.debug {
		hub.LK.Lock()
		for key, session := range hub.Sessions {
			println(fmt.Sprintf("session:%s,user:%s,count:%d", key, session.User, len(hub.Sessions)))
		}
		hub.LK.Unlock()
	}
}
