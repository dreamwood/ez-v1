package ws

import (
	"github.com/gorilla/websocket"
	"net/http"
)

var Upgrd = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}
