package ws

import (
	"encoding/json"
	"errors"
	"strings"
)

type Response struct {
	Content []byte
	Err     error
	Silent  bool
}

func NewSilentResponse() *Response {
	return &Response{Silent: true}
}

func NewTextResponse(content string) *Response {
	return &Response{Content: []byte(content)}
}

type Request struct {
	MessageType int
	Business    int
	Content     string
}

const (
	BusinessReg = 2 * iota
	BusinessLogout
)

func NewRequest() *Request {
	return &Request{}
}

func (r *Request) Decode(data []byte) {
	e := json.Unmarshal(data, r)
	if e != nil {
		println(e)
	}
}

func (r *Request) Encode() []byte {
	data, e := json.Marshal(r)
	if e != nil {
		println(e)
		return nil
	}
	return data
}

type ApiPushParam struct {
	SendType string `json:"sendType"`
	Target   string `json:"target"`
	Data     string `json:"data"`
}

func HandleNsqMessage(content []byte) error {
	// 数据解码
	// 数据格式 SendType:Target:Data
	// SendType: G=Group组发，S=Single单链接发送，U=User按用户发送，A=All
	// Target：目标ID KEY 或者TOKEN ,SendType=A时，target可以忽略，但是应该填写ALL填充
	splites := strings.Split(string(content), ":")
	if len(splites) != 3 {
		return errors.New("数据格式错误")
	}
	switch splites[0] {
	case "G":
		PushToGroup(splites[1], []byte(splites[2]))
	case "U":
		PushToUser(splites[1], []byte(splites[2]))
	case "A":
		PushToAll([]byte(splites[2]))
	default:
		//未知的发送格式
		println("未知的SendType")
	}

	return nil
}

func HandleApiMessage(param *ApiPushParam) error {
	// 数据解码
	// 数据格式 SendType:Target:Data
	// SendType: G=Group组发，S=Single单链接发送，U=User按用户发送，A=All
	// Target：目标ID KEY 或者TOKEN ,SendType=A时，target可以忽略，但是应该填写ALL填充
	switch param.SendType {
	case "G":
		PushToGroup(param.Target, []byte(param.Data))
	case "U":
		PushToUser(param.Target, []byte(param.Data))
	case "A":
		PushToAll([]byte(param.Data))
	default:
		//未知的发送格式
		println("未知的SendType")
	}

	return nil
}
