package ftp

import (
	"context"
	"gitee.com/dreamwood/ez-go/ez"
)

func init() {
	//web 文件服务的 ftp
	ez.Subscribe(ez.EventAfterServerRun, func(v interface{}, ctx context.Context) {
		go ez.StartFtp("./web", 10021, "ez", "ez")
	})
}
