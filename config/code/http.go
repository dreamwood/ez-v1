package code

const Success = 2001

const Error = 4000          //一般
const ErrorAuth = 4001      //登录过期
const ErrorAccess = 4003    //无妨问权限
const ErrorNotFound = 4004  //not found
const ErrorBlackList = 4005 //被拉黑禁用

const ErrorServer = 5000 //一般服务器错误
const ErrorMysql = 5001  //数据库错误
