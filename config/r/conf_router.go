package r

import (
	"fmt"
	"gitee.com/dreamwood/ez-go/ez"
)

const (
	Group = "/core"
	Admin = "/admin"

	Action_List         = "list"
	Action_Get          = "get"
	Action_Scan         = "scan"
	Action_Choice       = "choice"
	Action_Update       = "update"
	Action_Save         = "save"
	Action_Save_Many    = "save_many"
	Action_Delete       = "delete"
	Action_UnDelete     = "undelete"
	Action_Delete_Many  = "delete_many"
	Action_Destroy      = "destroy"
	Action_Destroy_Many = "destroy_many"
	Action_Copy         = "copy"
	Action_Help         = "help"
	Action_Tree         = "tree"
)

func R(url string) *ez.Router {
	return &ez.Router{Url: url}
}

func Api(url string) *ez.Router {
	return &ez.Router{Group: Group, Url: url}
}

func AdminApi(url string) *ez.Router {
	return &ez.Router{Group: Group, Prefix: Admin, Url: url}
}

func AdminList(modelName string) *ez.Router {
	return &ez.Router{Group: Group, Prefix: Admin, Url: fmt.Sprintf("/%s/%s", modelName, Action_List)}
}
func AdminGet(modelName string) *ez.Router {
	return &ez.Router{Group: Group, Prefix: Admin, Url: fmt.Sprintf("/%s/%s", modelName, Action_Get)}
}
func AdminScan(modelName string) *ez.Router {
	return &ez.Router{Group: Group, Prefix: Admin, Url: fmt.Sprintf("/%s/%s", modelName, Action_Scan)}
}
func AdminChoice(modelName string) *ez.Router {
	return &ez.Router{Group: Group, Prefix: Admin, Url: fmt.Sprintf("/%s/%s", modelName, Action_Choice)}
}
func AdminSave(modelName string) *ez.Router {
	return &ez.Router{Group: Group, Prefix: Admin, Url: fmt.Sprintf("/%s/%s", modelName, Action_Save)}
}
func AdminUpdate(modelName string) *ez.Router {
	return &ez.Router{Group: Group, Prefix: Admin, Url: fmt.Sprintf("/%s/%s", modelName, Action_Update)}
}
func AdminUpdateMany(modelName string) *ez.Router {
	return &ez.Router{Group: Group, Prefix: Admin, Url: fmt.Sprintf("/%s/%s", modelName, Action_Save_Many)}
}
func AdminDelete(modelName string) *ez.Router {
	return &ez.Router{Group: Group, Prefix: Admin, Url: fmt.Sprintf("/%s/%s", modelName, Action_Delete)}
}
func AdminUnDelete(modelName string) *ez.Router {
	return &ez.Router{Group: Group, Prefix: Admin, Url: fmt.Sprintf("/%s/%s", modelName, Action_UnDelete)}
}
func AdminDeleteMany(modelName string) *ez.Router {
	return &ez.Router{Group: Group, Prefix: Admin, Url: fmt.Sprintf("/%s/%s", modelName, Action_Delete_Many)}
}
func AdminDestroy(modelName string) *ez.Router {
	return &ez.Router{Group: Group, Prefix: Admin, Url: fmt.Sprintf("/%s/%s", modelName, Action_Destroy)}
}
func AdminDestroyMany(modelName string) *ez.Router {
	return &ez.Router{Group: Group, Prefix: Admin, Url: fmt.Sprintf("/%s/%s", modelName, Action_Destroy_Many)}
}
func AdminCopy(modelName string) *ez.Router {
	return &ez.Router{Group: Group, Prefix: Admin, Url: fmt.Sprintf("/%s/%s", modelName, Action_Copy)}
}
func AdminHelp(modelName string) *ez.Router {
	return &ez.Router{Group: Group, Prefix: Admin, Url: fmt.Sprintf("/%s/%s", modelName, Action_Help)}
}

func AdminTree(modelName string) *ez.Router {
	return &ez.Router{Group: Group, Prefix: Admin, Url: fmt.Sprintf("/%s/%s", modelName, Action_Tree)}
}
